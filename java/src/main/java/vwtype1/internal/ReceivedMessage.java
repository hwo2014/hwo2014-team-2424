package vwtype1.internal;

import com.google.gson.JsonElement;

public class ReceivedMessage {

    public final String msgType;
    public final JsonElement data;
    public final String gameId;
    public final int gameTick;

    public ReceivedMessage(String msgType, JsonElement data, String gameId, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameId = gameId;
        this.gameTick = gameTick;
    }

}
