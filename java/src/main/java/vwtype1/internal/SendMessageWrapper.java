package vwtype1.internal;

public class SendMessageWrapper {

    public final String msgType;
    public final Object data;

    public SendMessageWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public SendMessageWrapper(final SendMessage sendMessage) {
        this(sendMessage.msgType(), sendMessage.msgData());
    }

}
