package vwtype1.internal;

import com.google.gson.Gson;

public abstract class SendMessage {

    private static final Gson GSON = new Gson();

    public String toJson() {
        return GSON.toJson(new SendMessageWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();

}
