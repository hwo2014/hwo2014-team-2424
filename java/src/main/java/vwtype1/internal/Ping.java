package vwtype1.internal;

public class Ping extends SendMessage {

    public static final Ping INSTANCE = new Ping();

    @Override
    protected String msgType() {
        return "ping";
    }

}
