package vwtype1;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vwtype1.internal.*;

import java.io.*;
import java.net.Socket;

public class Bot {

    private final static Logger LOG = LoggerFactory.getLogger(Bot.class);

    public void go(final String host, final int port, final String name, final String key) throws IOException {
        LOG.info("Connecting to " + host + ":" + port + " as " + name + "/" + key);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        send(writer, new Join(name, key));

        final Gson gson = new Gson();

        JsonObject race = null;
        JsonObject myCar = null;

        boolean raceStarted = false;

        String line;
        while((line = reader.readLine()) != null) {
            final ReceivedMessage msgFromServer = gson.fromJson(line, ReceivedMessage.class);
            switch (msgFromServer.msgType) {
                case "carPositions":
                    Throttle throttle;
                    JsonArray carPositions = msgFromServer.data.getAsJsonArray();
                    if (isOnStraightPiece(race, myCar, carPositions)) {
                        throttle = new Throttle(0.8); // pedal to the metal on straights
                    } else {
                        throttle = new Throttle(0.5); // slow down a little bit on bends
                    }
                    send(writer, throttle);
                    break;
                case "join":
                    LOG.info("Joined");
                    break;
                case "yourCar":
                    myCar = msgFromServer.data.getAsJsonObject();
                    LOG.info("Got the {} car", myCar.get("color").getAsString());
                    break;
                case "gameInit":
                    race = msgFromServer.data.getAsJsonObject().getAsJsonObject("race");
                    LOG.info("Race init on {}", race.getAsJsonObject("track").get("name").getAsString());
                    break;
                case "gameEnd":
                    LOG.info("Race end");
                    raceStarted = false;
                    break;
                case "gameStart":
                    LOG.info("Race start");
                    raceStarted = true;
                    break;
                default:
                    if (raceStarted) {
                        throttle = new Throttle(0.1); // play it safe
                        send(writer, throttle);
                    } else {
                        send(writer, Ping.INSTANCE);
                    }
                    break;
            }
        }

        socket.close();
    }

    private void send(final PrintWriter writer, final SendMessage msg) {
        writer.println(msg.toJson());
        writer.flush();
    }

    private boolean isOnStraightPiece(final JsonObject race, final JsonObject myCar, final JsonArray carPositions) {
        int pieceIndex = -1;

        String myColor = myCar.get("color").getAsString();
        for(int i = 0; i < carPositions.size(); i++) {
            JsonObject carPosition = carPositions.get(i).getAsJsonObject();
            String carColor = carPosition.getAsJsonObject("id").get("color").getAsString();
            if (myColor.equals(carColor)) {
                pieceIndex = carPosition.getAsJsonObject("piecePosition").get("pieceIndex").getAsInt();
                break;
            }
        }

        JsonArray pieces = race.getAsJsonObject("track").getAsJsonArray("pieces");
        if (pieceIndex >= 0 && pieceIndex < pieces.size()) {
            JsonObject piece = pieces.get(pieceIndex).getAsJsonObject();
            return piece.has("length"); // it's a straight
        } else {
            LOG.warn("Unable to detect car position on track {}", carPositions);
            return false;
        }
    }

}

